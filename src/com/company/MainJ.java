package com.company;

import java.io.*;

public class MainJ {
	public static final String NAME = "fastqcut.";
	static String data = "!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";

	public static void main(String[] args) throws IOException {
		try (
				BufferedReader in = new BufferedReader(new FileReader(NAME + "fastq"));
				PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(NAME + "out")))
		) {
			String s1, s2, s3, s4, s5, s6;
			int p0 = Integer.parseInt(in.readLine());

			in.readLine();
			while ((s1 = in.readLine()) != null) {
				s2 = in.readLine();
				s3 = in.readLine();
				s4 = in.readLine();
				s5 = s2;
				s6 = s4;
				for (int i = 0; i < s4.length(); i++)
					if (data.indexOf(s4.charAt(i)) < p0) {
						s6 = s4.substring(0, i);
						s5 = s2.substring(0, i);
						break;
					}
				out.println(s1 + "\n" + s5 + "\n" + s3 + "\n" + s6);
			}
		}
	}
}

package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class MainD {
	public static final String NAME = "start.";

	public static void swap(boolean[] b1, boolean[] b2) {
		for (int i = 0; i < b1.length; i++) {
			boolean tmp = b1[i];
			b1[i] = b2[i];
			b2[i] = tmp;
		}
	}

	public static void main(String[] args) {
		try (
				BufferedReader in = new BufferedReader(new FileReader(NAME + "in"));
				PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(NAME + "out")))
		) {
			String[] split = in.readLine().split(" ");
			int m = Integer.parseInt(split[0]);
			int n = Integer.parseInt(split[1]);
			char[] let = new char[n];
			ArrayList<Integer>[] G = new ArrayList[n];
			for (int i = 0; i < n; i++)
				G[i] = new ArrayList<>();

			for (int i = 0; i < n; i++) {
				split = in.readLine().split(" ");
				let[i] = split[2].charAt(0);
				G[Integer.parseInt(split[0]) - 1].add(i);
				G[Integer.parseInt(split[1]) - 1].add(i);
			}

			String s = in.readLine();
			boolean[] used = new boolean[n];
			Arrays.fill(used, true);
			for (int i = s.length() - 1; i >= 0; i--) {
				char ch = s.charAt(i);
				boolean[] newUsed = new boolean[n];
				Arrays.fill(used, true);
				for (int i1 = 0; i1 < n; i1++) {
					if (!used[i1]) continue;
					for (int j = 0; j < G[i1].size(); j++)
						if (let[i1] == ch)
							newUsed[G[i1].get(j)] = true;
				}
				swap(used, newUsed);
			}


			for (int i = 0; i < 100; i++) {

			}

			int count = 0;
			for (int i = 0; i < n; i++)
				if (used[i])
					count++;
			out.print(count + " ");

			for (int i = 0; i < n; i++)
				if (used[i])
					out.write((i + 1) + " ");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

package com.company;

import java.io.*;
import java.math.BigDecimal;
import java.nio.Buffer;

public class MainA {
	public static final String NAME = "mutation.";
	public static String[] split;


	public static void main(String[] args) throws IOException {
		try(
				BufferedReader in = new BufferedReader(new FileReader(NAME + "in"));
		        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(NAME + "out")))
		) {
			int n,m;

			split = in.readLine().split(" ");
			n = Integer.parseInt(split[0]);
			m = Integer.parseInt(split[1]);
			for (int i = 0; i < m; i++) {
			   out.println(new BigDecimal(Double.toString(getAnswer(in.readLine(), in.readLine()))));
			}
		}
	}

	public static double getAnswer(String s1, String s2) {
		int n = s1.length();
		long x = 1,y = (long) Math.pow(n,n);
		for (int i = 0; i < n; i++) {
			x *= (s1.charAt(i) == s2.charAt(i)) ? n - 1 : 1;
		}
		return (double)x/y;
	}
}

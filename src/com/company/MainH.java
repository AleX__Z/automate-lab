package com.company;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Alex on 26.01.2015.
 */
public class MainH {
	static final int SEGS = 20;
	static final int ITS = 980;

	static int n;
	static Scanner in = new Scanner(System.in);

	static double query(int index, double x)
	{
		for (int i = 0; i < n; i++)
		{
			if (i == index)
				System.out.print(x + " ");

			else
				System.out.print("0 ");
		}
		System.out.println();

		double f = in.nextDouble();

		return f;
	}

	public static void main(String[] args) {
		n = in.nextInt();
		ArrayList<Double> ans = new ArrayList<>();
		for (int i = 0; i < n; i++)
		{
			double best = 1000;
			double bestPos = 0;
			for (int s = 0; s < SEGS; s++)
			{
				double left = 1. / SEGS * s;
				double right  = 1. / SEGS * (s + 1);
				for (int it = 0; it < ITS / SEGS / 2; it++)
				{
					double c1 = (2 * left + right) / 3;
					double c2 = (left + 2 * right) / 3;
					double f1 = query(i, c1);
					double f2 = query(i, c2);
					if (f1 < best)
					{
						best = f1;
						bestPos = left;
					}
					if (f1 < f2)
						right = c2;
					else
						left = c1;
				}
			}
			ans.add(bestPos);
		}
		for (int i = 0; i < n; i++)
			System.out.println(ans.get(i) + " ");
		System.out.println();
		double f;
		f = in.nextDouble();
		System.out.println( "minimum " + f );

	}


}

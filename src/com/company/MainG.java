package com.company;

import java.io.*;

public class MainG {
	public static final String NAME = "artificial.";
	public static final String[] Automates = new String[]{
			"3 2 L M\n" +
					"3 3 R M\n" +
					"2 4 R M\n" +
					"2 1 M M",
			"1 1 L M",
			"2 3 L M\n" +
					"3 2 R M\n" +
					"1 2 M R", "2 1 M M\n" +
			"3 4 M M\n" +
			"1 1 R L\n" +
			"1 4 L M",
			"3 2 M M\n" +
					"1 1 R M\n" +
					"2 3 M L", "2 2 R M\n" +
			"3 4 M M\n" +
			"3 5 L M\n" +
			"3 5 M M\n" +
			"1 2 M M",
			"2 4 R M\n" +
					"2 3 L M\n" +
					"2 4 M M\n" +
					"1 3 M M",
			"2 3 M M\n" +
					"3 3 L M\n" +
					"4 3 M M\n" +
					"5 2 R M\n" +
					"2 1 L M",
			"2 3 M M\n" +
					"4 3 R M\n" +
					"5 6 M M\n" +
					"6 6 L M\n" +
					"1 2 R M\n" +
					"5 1 L M",
			"2 3 L M\n" +
					"4 4 M M\n" +
					"2 4 R M\n" +
					"5 6 R M\n" +
					"1 1 L M\n" +
					"1 4 R M"};


	public static void main(String[] args) throws IOException {
		try (
				BufferedReader in = new BufferedReader(new FileReader(NAME + "in"));
				PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(NAME + "out")))
		) {
			out.print(Automates[Integer.parseInt(in.readLine()) - 1]);
		}
	}
}

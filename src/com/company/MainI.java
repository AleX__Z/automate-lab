package com.company;

import java.io.*;
import java.util.*;

/**
 * Created by Alex on 26.01.2015.
 */
public class MainI {
	public static final String NAME = "fasta-statistics.";
	public static Map<String, Integer> map = new HashMap<>();
	public static int sumContig = 0;


	public static void main(String[] args) throws IOException {
		try (
				BufferedReader in = new BufferedReader(new FileReader(NAME + "fasta"));
				PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(NAME + "out")))
		) {
			int count = 0;
			String tmp;
			String currectContig = null;
			while ((tmp = in.readLine()) != null) {
				if (tmp.startsWith(">")) {
					tmp = tmp + count++;
					map.put(tmp, 0);
					currectContig = tmp;
				} else {
					map.replace(currectContig, map.get(currectContig) + tmp.length());
					sumContig += tmp.length();
				}
			}
			ArrayList<Integer> values = new ArrayList<>(map.values());
			Collections.sort(values);
			Collections.reverse(values);

			int tmp2 = 0;
			for (Integer value1 : values) {
				tmp2 += value1;
				double d = (double) tmp2 / (double) sumContig;
				if (d >= 0.5) {
					out.println(value1);
					break;
				}
			}

			tmp2 = 0;
			for (Integer value : values) {
				tmp2 += value;
				double d = (double) tmp2 / (double) sumContig;
				if (d >= 0.9) {
					out.println(value);
					break;
				}
			}

			out.println(values.get(values.size()-1));
			out.println(values.get(0));
			out.printf("%.5f",(double)sumContig / map.size());
		}
	}

	public static class Pair<T, K> {
		public T first;
		public K second;

		public Pair(T first, K second) {
			this.first = first;
			this.second = second;
		}
	}
}

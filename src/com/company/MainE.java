package com.company;

import java.io.*;

public class MainE {
	public static final String NAME = "discrete.";

	public static void main(String[] args) throws IOException {
		try (
				BufferedReader in = new BufferedReader(new FileReader(NAME + "in"));
				PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(NAME + "out")))
		) {
			String[] split = in.readLine().split(" ");

			int n = Integer.parseInt(split[0]);
			int m = Integer.parseInt(split[1]);
			int[][] G = new int[n][2];

			for (int i = 0; i < n; i++) {
				split = in.readLine().split(" ");
				G[i][0] = Integer.parseInt(split[0]);
				G[i][1] = Integer.parseInt(split[1]);
			}

			for (int i = 0; i < n; i++) {
				G[i][0]--;
				G[i][1]--;
			}

			double[][][] o = new double[n][2][26];
			for (int i = 0; i < m; i++) {
				int tmp = 0, length;
				String inString, outString;
				split = in.readLine().split(" ");
				length = Integer.parseInt(split[0]);
				inString = split[1];
				outString = split[2];
				for (int j = 0; j < length; j++) {
					o[tmp][inString.charAt(j) - '0'][outString.charAt(j) - 'a'] += 1. / length;
					tmp = G[tmp][inString.charAt(j) - '0'];
				}
			}

			for (int i = 0; i < n; i++) {
				for (int j = 0; j < 2; j++) {
					double tmp = 0;
					int arg = 0;
					for (int k = 0; k < 26; k++)
						if (o[i][j][k] > tmp) {
							tmp = o[i][j][k];
							arg = k;
						}
					out.print((char) ('a' + arg) + " ");
				}
				out.println();
			}
		}
	}
}

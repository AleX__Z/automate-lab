package com.company;

public class Main {

    public static void main(String[] args) {
        /*boolean[] l1 = new boolean[]{true,false}, l2 = new boolean[]{false,true};
        System.out.println(Arrays.toString(l1));
        System.out.println(Arrays.toString(l2));
        System.out.println();
        swap(l1,l2);
        System.out.println(Arrays.toString(l1));
        System.out.println(Arrays.toString(l2));*/

        String s = "123";
        f(s);
        System.out.println(s);


    }

    public static void swap(boolean[] list1,boolean[] list2 ) {
        for (int i = 0; i < list1.length; i++) {
            boolean tmp = list1[i];
            list1[i] = list2[i];
            list2[i] = tmp;
        }
    }

    private static void f(String s) {
        s = s + 1;
    }
}

package com.company;

import java.io.*;

/**
 * Created by Alex on 26.01.2015.
 */
public class MainF {
	public static final String NAME = "continuous.";


	static final double EPS = 1e-9;

	static int compare(double a, double b) {
		if (Math.abs(a - b) < EPS)
			return 0;
		return a < b ? -1 : 1;
	}

	public static  void swap(double[] a, double[] b) {
	    double[] tmp = a.clone();
		a = b.clone();
		b = tmp;
	}

	public static  void swap(double a, double b) {
		double tmp = a;
		a = b;
		b = tmp;
	}

	static double[] gauss(double[][] a, double[] b) {
		int n = b.length;
		int m = a[0].length;
		for (int j = 0; j < m; j++) {
			int nonZero = j;
			for (int i = j; i < n; i++) {
				if (compare(a[i][j], 0) != 0) {
					nonZero = i;
					break;
				}
			}
			swap(a[j], a[nonZero]);
			swap(b[j], b[nonZero]);
			if (compare(a[j][j], 0) == 0)
				continue;
			for (int k = 0; k < m; k++) {
				if (k == j)
					continue;
				for (int i = 0; i < n; i++) {
					if (i == j)
						continue;
					a[i][k] -= a[i][j] / a[j][j] * a[j][k];
				}
			}
			for (int i = 0; i < n; i++)
				if (i != j)
					b[i] -= a[i][j] / a[j][j] * b[j];
			for (int i = 0; i < n; i++)
				if (i != j)
					a[i][j] = 0;

		}
		for (int i = 0; i < m; i++)
			if (compare(a[i][i], 0) == 0) {
				b[i] = 0;
			} else {
				b[i] /= a[i][i];
			}
		return b;
	}

	public static void main(String[] args) throws IOException {
		try (
				BufferedReader in = new BufferedReader(new FileReader(NAME + "in"));
				PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(NAME + "out")))

		) {
			String [] split = in.readLine().split(" ");
			int n, m;
			n = Integer.parseInt(split[0]);
			m = Integer.parseInt(split[1]);
			int [][] G = new int[n][2];

			double[][] sum = new double[2*n][2*n];
			double[] rhs  = new double[2*n];

			for (int i = 0; i < n; i++) {
				split = in.readLine().split(" ");
				G[i][0] = Integer.parseInt(split[0]);
				G[i][1] = Integer.parseInt(split[1]);
				G[i][0]--;
				G[i][1]--;
			}
			for (int i = 0; i < m; i++) {
				split = in.readLine().split(" ");
				int me = 0;
				int len;
				String inf;

				len = Integer.parseInt(split[0]);
				inf = split[1];
				double[] path = new double[2*n];
				for (int j = 0; j < len; j++) {
					double now;
					now = Double.parseDouble(split[2 + j]);
					path[2 * me + inf.charAt(j) - '0'] += 1. / len;
					me = G[me][inf.charAt(j) - '0'];

					for (int j1 = 0; j1 < 2 * n; j1++) {
						for (int k = 0; k < 2 * n; k++) {
							sum[j1][k] += path[j1] * path[k] * len;
							sum[k][j1] += path[j1] * path[k] * len;
						}
						rhs[j1] += 2 * path[j1] * now;
					}
				}
			}


			double[] ans = gauss(sum, rhs);


			for (int i = 0; i < n; i++) {
				out.printf("%.6f",ans[2 * i]);
				out.print(" ");
				out.printf("%.6f", ans[2 * i + 1]);
				out.println();
			}
		}
	}



}

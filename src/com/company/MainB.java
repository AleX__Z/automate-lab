package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Alex on 26.01.2015.
 */
public class MainB {
	public static final String NAME = "crossover.";
	public static ArrayList<String> A = new ArrayList<>();
	static String s;

	public static boolean test1(String s1, String s2, String s3) {
		int count = -1;
		int l = s1.length();
		for (int i = 0; i < l; i++) {
			if (s1.charAt(i) != s3.charAt(i)) {
				count = i;
				break;
			}
		}
		if (count == -1) return true;

		for (int i = count; i < l; i++)
			if (s2.charAt(i) != s3.charAt(i))
				return false;

		return true;
	}

	public static boolean test2(String s1, String s2, String s3) {
		int count = -1;
		int l = s1.length();
		for (int i = 0; i < l; i++) {
			if (s1.charAt(i) != s3.charAt(i)) {
				count = i;
				break;
			}
		}
		if (count == -1) return true;

		int newCount = -1;
		for (int i = count; i < l; i++)
			if (s2.charAt(i) != s3.charAt(i)) {
				newCount = i;
				break;
			}
		if (newCount == -1) return true;


		for (int i = newCount; i < l; i++)
			if (s1.charAt(i) != s3.charAt(i))
				return false;

		return true;

	}

	public static boolean test3(String s1, String s2, String s3) {
		for (int i = 0, size = s1.length(); i < size; i++)
			if (s1.charAt(i) != s3.charAt(i) && s2.charAt(i) != s3.charAt(i))
				return false;
		return true;
	}

	public static String test() {
		Boolean[] b = new Boolean[3];
		Arrays.fill(b,false);
		for (String aA : A) {
			for (String aA1 : A) {
				if (!b[0]) b[0] = test1(aA, aA1, s);
				if (!b[1]) b[1] = test2(aA, aA1, s);
				if (!b[2]) b[2] = test3(aA, aA1, s);
				if (b[0] && b[1] && b[2]) break;
			}
		}

		return Arrays.stream(b).map(x-> x ? "YES" : "NO").reduce((x,y) -> x + "\n" + y).get();
	}

	public static void main(String[] args) throws IOException {
		try (
				BufferedReader in = new BufferedReader(new FileReader(NAME + "in"));
				PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(NAME + "out")))
		) {
			String[] split = in.readLine().split(" ");
			int m = Integer.parseInt(split[0]), n = Integer.parseInt(split[1]);
			for (int i = 0; i < m; i++) {
				A.add(in.readLine());
			}
			s = in.readLine();

			String tmp = test();
			out.print(test());

			/*if (A.contains(s)) {
				out.print("YES\nYES\nYES");
			} else {
				out.print(test());
			}*/

		}
	}
}

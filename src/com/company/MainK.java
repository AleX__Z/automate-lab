package com.company;

import com.sun.javafx.scene.control.skin.TableCellSkin;

import java.io.*;
import java.util.*;

/**
 * Created by Alex on 26.01.2015.
 */
public class MainK {
	public static final String NAME = "count-entries.";
	static Map<String, Integer> map = new TreeMap<>();

	public static void f(int k, String s) {
		for (int i = 0; i < s.length() - k + 1; i++) {
			String tmp = s.substring(i, i + k);
			if (!map.containsKey(tmp)) {
				map.put(tmp, 1);
			} else {
				map.put(tmp, map.get(tmp) + 1);
			}
		}
	}

	public static void main(String[] args) throws IOException {
		try (
				BufferedReader in = new BufferedReader(new FileReader(NAME + "in"));
				PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(NAME + "out")))
		) {
			int k = Integer.parseInt(in.readLine());
			int n = Integer.parseInt(in.readLine());
			for (int i = 0; i < n; i++) {
				f(k, in.readLine());
			}


			for (String s : map.keySet()) {
				out.println(s + " " + map.get(s));
			}

		}
	}
}

package com.company;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class MainC {
	private static int k, newK;
	private static byte[] flag;
	private static Node[] nodes;
	private static PrintWriter out;
	private static StringBuilder answer;
	private static HashMap<Integer, Integer> invMap;
	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(new FileReader("trees.in"));
		out = new PrintWriter("trees.out");
		k = in.nextInt();
		nodes = new Node[k+1];
		for (int i = 1; i <= k; i++) nodes[i] = new Node();
		String str;
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		invMap = new HashMap<Integer, Integer>();
		int count = 1;
		int x;
		for (int i = 1; i <= k; i++) {
			str = in.next();
			if (str.equals("choice")) {
				x = in.nextInt();
				if (map.containsKey(x)) {
					nodes[i].id = i;
					nodes[i].pred = map.get(x);
				} else {
					map.put(x, count);
					invMap.put(count, x);
					nodes[i].id = i;
					nodes[i].pred = count;
					count++;
				}
				x = in.nextInt();
				nodes[i].left = nodes[x];
				nodes[x].par = nodes[i];
				x = in.nextInt();
				nodes[i].right = nodes[x];
				nodes[x].par = nodes[i];
			} else {
				x = in.nextInt();
				nodes[i].id = i;
				nodes[i].pred = x;
				nodes[i].isLeaf = true;
			}
		}
		flag = new byte[count + 1];
		byte a = -1;
		Arrays.fill(flag, a);
		dfs(nodes[1]);
		answer = new StringBuilder();
		newK = 0;
		giveIds(nodes[1]);
		out.println(newK);
		writeAns(nodes[1]);
		out.close();
	}

	private static void giveIds(Node v) {
		newK++;
		v.id = newK;
		if (v.isLeaf) return;
		giveIds(v.left);
		giveIds(v.right);
	}

	private static void writeAns(Node v) {
		if (v.isLeaf) {
			out.println("leaf " + v.pred);
		} else {
			out.println("choice " + invMap.get(v.pred) + " " + v.left.id + " " + v.right.id);
			writeAns(v.left);
			writeAns(v.right);
		}
	}

	private static void dfs(Node v) {
		if (v.isLeaf) return;
		if (flag[v.pred] == 0) {
			if (v.par.left.id == v.id) {
				v.par.left = v.left;
				v.left.par = v.par;
			}
			else {
				v.par.right = v.left;
				v.left.par = v.par;
			}
		}
		if (flag[v.pred] == 1) {
			if (v.par.left.id == v.id) {
				v.par.left = v.right;
				v.right.par = v.par;
			}
			else {
				v.par.right = v.right;
				v.right.par = v.par;
			}
		}
		byte temp;
		if (flag[v.pred] != 1) {
			temp = flag[v.pred];
			flag[v.pred] = 0;
			dfs(v.left);
			flag[v.pred] = temp;
		}
		if (flag[v.pred] != 0) {
			temp = flag[v.pred];
			flag[v.pred] = 1;
			dfs(v.right);
			flag[v.pred] = temp;
		}

	}

	private static class Node {
		int id;
		int pred;
		Node left, right, par;
		boolean isLeaf;

		public Node(int p, int id) {
			pred = p;
			this.id = id;
			isLeaf = false;
		}

		public Node(){}
	}
	public static class Assign
	{


		private static int[] assignment(int[][] a)
		{
			int n = a.length;
			boolean[][] notallow = new boolean[n][n];

			int[] m = new int[n];

			while (true)
			{
				int max = -1, maxi = -1, maxj = -1;

				for (int i = 0; i < n; i++)
				{
					for (int j = 0; j < n; j++)
					{
						if (notallow[i][j]) continue;
						if (a[i][j] > max)
						{
							max = a[i][j];
							maxi = i; maxj = j;
						}
					}
				}

				if (max == -1) break;

				m[maxi] = maxj;

				for (int i = 0; i < n; i++)
				{
					notallow[maxi][i] = true;
					notallow[i][maxj] = true;
				}
			}
			return m;
		}



		public static String createSuperString(ArrayList<String> strings)
		{
			int n = strings.size();


			int[][] overlaps = new int[n][n];
			for (int i = 0; i < n; i++)
				for (int j = 0; j < n; j++)
					overlaps[i][j] = overlap(strings.get(i), strings.get(j));

			int[] assign = assignment(overlaps);


			ArrayList<ArrayList<Integer>> cycles
					= new ArrayList<ArrayList<Integer>>();
			ArrayList<Integer> cycle = new ArrayList<Integer>();
			boolean[] mark = new boolean[assign.length];

			for (int i = 0; i < assign.length; i++)
			{
				if (mark[i]) continue;

				cycle.add(i);
				mark[i] = true;

				if (assign[i] == cycle.get(0))
				{
					cycles.add(cycle);
					cycle = new ArrayList<Integer>();
					i = 0;
				}
				else
				{
					i = assign[i] - 1;
				}
			}


			ArrayList<String> superstrings = new ArrayList<String>();
			for (ArrayList<Integer> c : cycles)
			{
				String str = "";
				ArrayList<Integer> ovs = new ArrayList<Integer>();

				for (int i = 0; i < c.size() - 1; i++)
					ovs.add(overlaps[c.get(i)][c.get(i+1)]);

				int min = overlaps[c.get(c.size()-1)][c.get(0)];
				int shift = 0;

				for (int i = 0; i < ovs.size(); i++)
					if (ovs.get(i) < min)
					{
						min = ovs.get(i);
						shift = i + 1;
					}

				Collections.rotate(c, -shift);

				for (int i = 0; i < c.size() - 1; i++)
					str += prefix(strings.get(c.get(i)),
							overlaps[c.get(i)][c.get(i+1)]);
				str += strings.get(c.get(c.size()-1));
				superstrings.add(str);
			}


			StringBuilder superstring = new StringBuilder();
			for (String str : superstrings)
				superstring.append(str);

			return superstring.toString();
		}


		private static String prefix(String s1, int ov)
		{
			return s1.substring(0, s1.length() - ov);
		}


		private static int overlap(String s1, String s2)
		{
			int s1last = s1.length() - 1;
			int s2len = s2.length();
			int overlap = 0;
			for (int i = s1last, j = 1; i > 0 && j < s2len; i--, j++)
			{
				String suff = s1.substring(i);
				String pref = s2.substring(0, j);
				if (suff.equals(pref)) overlap = j;
			}
			return overlap;
		}
	}


	public static class Greedy
	{

		public static String createSuperString(ArrayList<String> strings)
		{
			while (strings.size() > 1)
			{
				int maxoverlap = 0;
				String msi = strings.get(0), msj = strings.get(1);
				for (String si : strings)
					for (String sj : strings)
					{
						if (si.equals(sj)) continue;
						int curoverlap = overlap(si, sj);
						if (curoverlap > maxoverlap)
						{
							maxoverlap = curoverlap;
							msi = si; msj = sj;
						}
					}

				strings.add(merge(msi, msj, maxoverlap));
				strings.remove(msi);
				strings.remove(msj);
			}
			return strings.get(0);
		}


		private static int overlap(String s1, String s2)
		{
			int s1last = s1.length() - 1;
			int s2len = s2.length();
			int overlap = 0;
			for (int i = s1last, j = 1; i > 0 && j < s2len; i--, j++)
			{
				String suff = s1.substring(i);
				String pref = s2.substring(0, j);
				if (suff.equals(pref)) overlap = j;
			}
			return overlap;
		}


		private static String merge(String s1, String s2, int len)
		{
			s2 = s2.substring(len);
			return s1 + s2;
		}
	}
}